package qsh

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"net/url"
	"sort"
	"strings"
)

func Hash(canonical string) string {
	fmt.Printf("canonical: %q\n", canonical)

	k := sha256.New()
	k.Write([]byte(canonical))
	h := k.Sum(nil)
	return hex.EncodeToString(h)
}

func Request(method string, baseUrl string, uri string) string {
	k := strings.IndexByte(uri, '?')

	var q string
	if k >= 0 {
		q = uri[k+1:]
		uri = uri[:k]
	}
	buf := &bytes.Buffer{}

	buf.WriteString(strings.ToUpper(method))
	buf.WriteByte('&')
	buf.WriteString(Path(baseUrl, uri))
	buf.WriteByte('&')
	query(buf, q)

	return buf.String()
}

func Query(q string) string {
	buf := &bytes.Buffer{}
	query(buf, q)
	return buf.String()
}

func Path(baseUrl, path string) string {
	if strings.HasPrefix(path, baseUrl) {
		path = path[len(baseUrl):]
	}
	for strings.HasPrefix(path, "/") {
		path = path[1:]
	}
	for strings.HasSuffix(path, "/") {
		path = path[:len(path)-1]
	}
	if len(path) == 0 {
		return "/"
	}

	return "/" + strings.Replace(path, "&", "%26", -1)
}

func query(dest *bytes.Buffer, q string) {
	tbl, err := url.ParseQuery(q)
	if err != nil {
		return
	}
	var keys sort.StringSlice
	for k, _ := range tbl {
		if k != "jwt" {
			keys = append(keys, k)
		}
	}
	sort.Sort(keys)

	any := false
	for _, k := range keys {
		if any {
			dest.WriteByte('&')
		}
		any = true

		if len(tbl[k]) > 1 {
			panic("multiple values not supported")
		}
		encoded(dest, k)
		dest.WriteByte('=')
		encoded(dest, tbl[k][0])
	}
}

func encoded(dest *bytes.Buffer, src string) {
	for _, b := range []byte(src) {
		if !noEncodingNeeded[b] {
			fmt.Fprintf(dest, "%%%02X", b)
		} else {
			dest.WriteByte(b)
		}
	}
}

// c.f. https://en.wikipedia.org/wiki/Percent-encoding

var noEncodingNeeded = [256]bool{
	'A': true,
	'B': true,
	'C': true,
	'D': true,
	'E': true,
	'F': true,
	'G': true,
	'H': true,
	'I': true,
	'J': true,
	'K': true,
	'L': true,
	'M': true,
	'N': true,
	'O': true,
	'P': true,
	'Q': true,
	'R': true,
	'S': true,
	'T': true,
	'U': true,
	'V': true,
	'W': true,
	'X': true,
	'Y': true,
	'Z': true,
	'a': true,
	'b': true,
	'c': true,
	'd': true,
	'e': true,
	'f': true,
	'g': true,
	'h': true,
	'i': true,
	'j': true,
	'k': true,
	'l': true,
	'm': true,
	'n': true,
	'o': true,
	'p': true,
	'q': true,
	'r': true,
	's': true,
	't': true,
	'u': true,
	'v': true,
	'w': true,
	'x': true,
	'y': true,
	'z': true,
	'0': true,
	'1': true,
	'2': true,
	'3': true,
	'4': true,
	'5': true,
	'6': true,
	'7': true,
	'8': true,
	'9': true,
	'-': true,
	'_': true,
	'.': true,
	'~': true,
}
