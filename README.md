# Query String Hash

This library implements canonical requests, including canonicalizing
query strings, compatible with the Atlassian API as documented
here: https://developer.atlassian.com/cloud/bitbucket/query-string-hash/
