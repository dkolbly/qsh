package qsh

import (
	"testing"
)

func TestCanonicalPath(t *testing.T) {

	for _, example := range [][]string{
		{
			"https://addon.example.com/jira-connector",
			"https://addon.example.com/jira-connector",
			"/",
		},
		{
			"https://addon.example.com/jira-connector",
			"https://addon.example.com/jira-connector/issue",
			"/issue",
		},
		{
			"https://addon.example.com/jira-connector",
			"https://addon.example.com/jira-connector/title&description",
			"/title%26description",
		},
		{
			"https://example.atlassian.net/",
			"https://example.atlassian.net/rest/api/2/issue/",
			"/rest/api/2/issue",
		},
	} {
		c := Path(example[0], example[1])
		if c != example[2] {
			t.Errorf("expected %q got %q", example[2], c)
		}
	}

}

func TestCanonicalQuery(t *testing.T) {

	for _, example := range [][]string{
		{
			"",
			"",
		},
		{
			"b=2&a=1&c=3",
			"a=1&b=2&c=3",
		},
		{
			"expand=b,a,c",
			"expand=b,a,c",
		},
	} {
		c := Query(example[0])
		if c != example[1] {
			t.Errorf("expected %q got %q", example[1], c)
		}
	}

}
